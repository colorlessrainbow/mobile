'use strict';

import React, {Component} from 'react';
import ReactNative from 'react-native';
const styles = require('../styles.js');
const constants = styles.constants;

const { StyleSheet, Text, TextInput, View, TouchableHighlight} = ReactNative;

class AppName extends Component {
  render() {
    return (
      <View style={styles.appTitle}>
        <Text style={styles.appTextTitle}>Weather Application</Text>
      </View>
    );
  }
}


module.exports = AppName;
