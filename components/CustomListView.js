import React from 'react';
import { View, ListView, StyleSheet, Text, TouchableHighlight } from 'react-native';
const styles = require('../styles.js');
const constants = styles.constants;

class CustomListView extends React.Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = { dataSource: ds.cloneWithRows(['Alba', 'Arad', 'Arges', 'Bacau', 'Bihor', 'Bistrita-Nasaud', 'Botosani', 'Brasov', 'Braila', 'Bucuresti', 'Buzau', 'Caras-Severin',
    'Calarasi', 'Cluj', 'Constant', 'Covasna', 'Dambovita', 'Dolj', 'Galati', 'Giurgiu', 'Harghita', 'Hunedoara', 'Ialomita', 'Iasi', 'Ilfov', 'Maramures', 'Mehedinti', 'Mures', 'Neamt',
    'Olt', 'Prahova', 'Satu Mara', 'Salaj', 'Sibiu', 'Suceava', 'Teleorman', 'Timis', 'Tulcea', 'Vaslui', 'Valcea', 'Vrancea'])};
    this.renderRow = this.renderRow.bind(this);
  };

  renderRow(filter) {
      var onPress = () => this.props.navigator.push({id: 'navigation', filter:filter});
      return <Text style={styles.listElementsText} onPress={onPress}>{filter}</Text>;
    };

  render() {
    return (
      <View style={styles.listViewContainer}>
        <Text style={styles.listTitleText}>{this.props.text}</Text>
        <ListView dataSource={this.state.dataSource} renderRow={this.renderRow}/>
      </View>
    );
  }
}

export default CustomListView;
