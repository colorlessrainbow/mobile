import React, { Component, PropTypes } from 'react';
import { View, Text, StyleSheet, Linking, ListView, ListItem, Navigator } from 'react-native';

import CustomTextInput from './CustomTextInput';
import AppName from './AppName';
import CustomListView from './CustomListView';
import AndroidBridgeModule from '../components/native_module/';
const styles = require('../styles.js')
const constants = styles.constants;

export default class Home extends Component {
  constructor(props) {
    super(props);
  };

  handleClick = (data) => {
    AndroidBridgeModule.log("clicked!");
    //AndroidBridgeModule.sendEmail(this.textInput.state.inputText);
    Linking.openURL("mailto:moba.weather@gmail.com?subject=Suggestions&body=" + this.textInput.state.inputText);
  };

  render() {
    return (
      <View style={styles.container}>
        <AppName text="Weather Application"/>
        <View style={{backgroundColor: styles.container.backgroundColor, height: 10}} />
        <CustomTextInput ref={(CustomTextInput) => this.textInput=CustomTextInput} text="Send email!" placeholder="Make improvement suggestion" onClick={this.handleClick}/>
        <View style={{backgroundColor: styles.container.backgroundColor, height: 10}} />
        <CustomListView text="Choose your county:" navigator={this.props.navigator} />
      </View>
    )
  }
};

module.export = Home;
