import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, ListView, ListItem, Navigator } from 'react-native';

import Home from './Home';
import WeatherSettings from './WeatherSettings'
import AndroidBridgeModule from '../components/native_module/';
const styles = require('../styles.js')
const constants = styles.constants;

export default class CodeSharing extends Component {
  constructor(props) {
    super(props);
  };

  renderScene(route, nav) {
    switch (route.id) {
      case 'welcome':
        return <Home navigator={nav} />;
      case 'navigation':
        return <WeatherSettings navigator={nav} filter={route.filter} />;
      default:
          console.log("Something went wrong.");
    }
  }

  render() {
    return (
      <Navigator initialRoute={{id: 'welcome'}} renderScene={this.renderScene} configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight}/>
    );
  }
};
