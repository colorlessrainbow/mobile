import React, { Component } from 'react';
import { View, Text, Navigator } from 'react-native';

import Button from './Button';
const styles = require('../styles.js');
const constants = styles.constants;

export default class WeatherSettings extends Component
{
  constructor(props) {
    super(props);
    this.goBack = this.goBack.bind(this);
  };

  goBack() {
    this.props.navigator.pop();
  };

render() {
  return (
    <View style={styles.weatherSettingContainer}>
      <Text style={styles.weatherSettingTitle}>Welcome to the settings page!</Text>
      <View style={{backgroundColor: styles.container.backgroundColor, height: 10}} />
      <Text style={styles.weatherSettingTitle}>County selected: {this.props.filter}</Text>
      <View style={{backgroundColor: styles.container.backgroundColor, height: 15}} />
      <Text style={styles.weatherSettingItems}>Type in city: </Text>
      <View style={{backgroundColor: styles.container.backgroundColor, height: 410}} />
      <Button onClick={this.goBack}/>
    </View> )
  }
}

module.export = WeatherSettings;
