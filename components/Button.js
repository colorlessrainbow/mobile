'use strict';

import React, {Component} from 'react';
import ReactNative from 'react-native';
const styles = require('../styles.js');
const constants = styles.constants;

const { StyleSheet, Text, TextInput, View, TouchableHighlight} = ReactNative;

class Button extends Component {
  render() {
    return (
      <View>
        <TouchableHighlight
          style={styles.buttonContainer}
          onPress={this.props.onClick}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableHighlight>
      </View>
    );
  }
}


module.exports = Button;
