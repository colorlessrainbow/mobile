'use strict';

import React, {Component} from 'react';
import ReactNative from 'react-native';
const styles = require('../styles.js');
const constants = styles.constants;

const { StyleSheet, Text, TextInput, View, TouchableHighlight} = ReactNative;

class CustomTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {inputText: ''};
  };

  render() {
    return (
      <View style={styles.action}>
        <TextInput
          style={styles.textInput}
          placeholder={this.props.placeholder}
          onChangeText={(text) => {this.setState({inputText:text})}}
        />

        <TouchableHighlight
          underlayColor={constants.actionColor}
          onPress={this.props.onClick}>
          <Text style={styles.actionText}>{this.props.text}</Text>
        </TouchableHighlight>
      </View>
    );
  }
}


module.exports = CustomTextInput;
