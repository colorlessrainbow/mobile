/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 import React, { AppRegistry } from 'react-native';
 import CodeSharing from './components';

 AppRegistry.registerComponent('MyWeatherApp', () => CodeSharing);;
