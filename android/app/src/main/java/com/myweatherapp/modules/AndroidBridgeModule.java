package com.myweatherapp.modules;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.nfc.Tag;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 0xa on 11/12/16.
 */

public class AndroidBridgeModule  extends ReactContextBaseJavaModule {

    private Context context;
    private static final String TAG = "AndroidBridgeModule";

    public AndroidBridgeModule(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
    }

    @Override
    public String getName() {
        return TAG;
    }


    @ReactMethod
    public void log(String message) {
        Log.d(TAG, "log: "+message);
    }

    @ReactMethod
    public void sendEmail(String body)
    {
        String[] emails = {"moba.weather@gmail.com"};
        String subject = "Improvement suggestions";
        String message = body;

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, emails);
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        // need this to prompts email client only
        email.setType("message/rfc822");

        context.startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }
}