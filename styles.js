const React = require('react-native')
const {StyleSheet} = React
const constants = {
  actionColor: 'transparent'
};

var styles = StyleSheet.create({
  container: {
    backgroundColor: '#8000800A',
    flexDirection: 'column',
    flex: 1,
  },

  weatherSettingContainer: {
    backgroundColor: "#8000800A",
    flexDirection: 'column',
    flex: 1,
  },

  weatherSettingTitle: {
    color: "#800080",
    fontSize: 20,
    textAlign: 'center'
  },

  weatherSettingItems: {
    color: "#800080",
    fontSize: 15,
    textAlign: 'left'
  },

  buttonContainer: {
    backgroundColor: "#80008026",
    borderColor: "#800080",
    borderWidth: 1.5,
    borderRadius:10,
    height: 50,
    flex: 1
  },

  buttonText: {
    color: "#800080",
    fontSize: 30,
    textAlign: 'center'
  },

  center: {
    textAlign: 'center',
  },
  actionText: {
    color: '#800080',
    fontSize: 17,
    textAlign: 'center',
  },
  action: {
    backgroundColor: "#00800026",
    borderColor: '#800080',
    borderWidth: 1.5,
    paddingLeft: 16,
    paddingTop: 14,
    paddingBottom: 16,
    borderRadius: 10
  },

  textInput: {
    color: "#008000",
    fontSize: 15,
    width: 300,
    height: 60
  },

  appTitle: {
    backgroundColor: "#80008026",
    borderColor: "#800080",
    borderWidth: 1.5,
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius:10
  },

  appTextTitle: {
    color: "#800080",
    fontSize: 30,
    textAlign: 'center'
  },

  listViewContainer: {
    backgroundColor: "#80008026",
    borderColor: "#800080",
    height: 350,
    borderWidth: 1.5,
    paddingLeft: 16,
    paddingTop: 14,
    paddingBottom: 16,
    borderRadius:10
  },

  listTitleText: {
    color: "#800080",
    fontSize: 20,
    textAlign: 'center'
  },

  listElementsText: {
    color: "#800080",
    fontSize: 15,
    textAlign: 'center'
  }
})

module.exports = styles
module.exports.constants = constants;
