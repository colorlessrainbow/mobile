package com.example.a0xa.exampleapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ListElement extends AppCompatActivity {
    TextView countyTitleName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listelement);
        countyTitleName = (TextView) findViewById(R.id.countyName);
        Intent myIntent = getIntent();
        countyTitleName.setText(myIntent.getStringExtra("name"));
    }
}
